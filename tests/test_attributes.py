import pytest

import protein_folding_energy


@pytest.mark.parametrize("attribute", ["__version__"])
def test_attribute(attribute):
    assert getattr(protein_folding_energy, attribute)


def test_main():
    import protein_folding_energy

    assert protein_folding_energy
