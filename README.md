# protein-folding-energy

[![conda](https://img.shields.io/conda/dn/ostrokach-forge/protein-folding-energy.svg)](https://anaconda.org/ostrokach-forge/protein-folding-energy/)
[![docs](https://img.shields.io/badge/docs-v0.1.0-blue.svg)](https://ostrokach.gitlab.io/protein-folding-energy/v0.1.0/)
[![pipeline status](https://gitlab.com/ostrokach/protein-folding-energy/badges/v0.1.0/pipeline.svg)](https://gitlab.com/ostrokach/protein-folding-energy/commits/v0.1.0/)
[![coverage report](https://gitlab.com/ostrokach/protein-folding-energy/badges/v0.1.0/coverage.svg)](https://ostrokach.gitlab.io/protein-folding-energy/v0.1.0/htmlcov/)

Collection of protein folding energy datasets.
